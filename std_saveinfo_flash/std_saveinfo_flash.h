#ifndef __SAVEINFO_FLASH_H
#define	__SAVEINFO_FLASH_H

#include "stm32f10x_conf.h"

/*==============================================================================*/
#define ProgramTimeout        ((uint32_t)0x00002000)

#define FLASH_CAPACITY   64                      //total flash capacity size(KB)

#define STM32FLASH_SIZE  FLASH_CAPACITY*1024     //size(Byte)

#define STM32FLASH_BASE  FLASH_BASE

#define STM32FLASH_END   (STM32FLASH_BASE | STM32FLASH_SIZE)

/*total flash memory 6K/32K/64K/128K is 0x400U, and 256K/384K/512K is 0x800U*/

#if FLASH_CAPACITY <256
    #define STM32FLASH_PAGE_SIZE 0x400U
#else
    #define STM32FLASH_PAGE_SIZE 0x800U
#endif

#define STM32FLASH_PAGE_NUM  (STM32FLASH_SIZE / STM32FLASH_PAGE_SIZE)

#define Address_Begin  0x0800FC00                //for 103c8 serial



void FLASH_Init(void);
uint32_t FLASH_Read(uint32_t Address, void *Buffer, uint32_t Size);
uint32_t FLASH_Write(uint32_t Address, const uint16_t *Buffer, uint16_t NumToWrite);


#endif

