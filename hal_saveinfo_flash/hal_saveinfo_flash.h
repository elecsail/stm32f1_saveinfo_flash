#ifndef __SAVEINFO_FLASH_H
#define	__SAVEINFO_FLASH_H

#include "stm32f1xx_hal.h"

/*==============================================================================*/

#define FLASH_CAPACITY   64                      //total flash capacity size(KB) of chip

#define STM32FLASH_SIZE  FLASH_CAPACITY*1024     //size(Byte)

#define STM32FLASH_BASE  FLASH_BASE

#define STM32FLASH_END   (STM32FLASH_BASE | STM32FLASH_SIZE)

#define STM32FLASH_PAGE_SIZE FLASH_PAGE_SIZE

#define STM32FLASH_PAGE_NUM  (STM32FLASH_SIZE / STM32FLASH_PAGE_SIZE)

#define Address_Begin  0x0800FC00                //for stm32f103c8t6 chip 



void FLASH_Init(void);
uint32_t FLASH_Read(uint32_t Address, void *Buffer, uint32_t Size);
uint32_t FLASH_Write(uint32_t Address, const uint16_t *Buffer, uint16_t NumToWrite);


#endif

