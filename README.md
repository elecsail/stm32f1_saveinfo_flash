# stm32f1_saveinfo_flash

此工程适用于stm32f1系列单片机，功能为内部flash模拟eeprom。使用函数可以为HAL库函数，也可以为标准库函数，放在两个不同文件夹内，参照文件夹头部命名；同样方便移植到其他芯片上。

使用说明：

1.添加.c和.h到Keil工程；

2.在头文件里定义芯片flash大小，#define FLASH_CAPACITY  64  ，单位KB，按自己使用芯片存储容量更改即可；

3.在头文件里修改Address_Begin，即最后一页地址。若最后一页不足以存储参数，也可以用前页地址。在.c文件里列出了三款不同容量芯片的最后页的地址作为参考。此宏定义仅为了主函数使用，若不用不修改也可以。eg:FLASH_Write(Address_Begin,&write_flash,5);即往此地址处写入五个16bit的数据，write_flash为自定义的uint16_t数组。

4.uint32_t FLASH_Write(uint32_t Address, const uint16_t *Buffer, uint16_t NumToWrite)，此函数为写函数，每次必须写入16bit数据，其中NumToWrite参数代表的是要写入多少个16bit数据；

5.uint32_t FLASH_Read(uint32_t Address, void *Buffer, uint32_t Size)，此函数为读函数，单位字节，Size代表需要读取的字节数，返回值为读取成功的字节数；